# Anime

## Input

### On create of anime

- [x] (Required) Title
- [x] Ratings (upon 10, whole number)
- [x] Already completed checkbox

### Dashboard
- [x] Complete button
- [ ] Search
- [ ] Sorting
  - [ ] by release date

## Features

- [ ] Fetching data from some godly API
  - [ ] Total number of episodes
  - [ ] Year of release
  - [ ] Sypnosis
  - [ ] Image (cover)
- [ ] On click of complete button
  - [ ] request for ratings
  - [ ] Save timestamp

## API proposed

- https://kitsune.readthedocs.io/en/latest/api.html
